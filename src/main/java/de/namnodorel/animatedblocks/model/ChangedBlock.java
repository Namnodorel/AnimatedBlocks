package de.namnodorel.animatedblocks.model;

import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;

public class ChangedBlock implements ConfigurationSerializable{

    private Integer relX;
    private Integer relY;
    private Integer relZ;
    private Material blockType;
    private byte data;

    public ChangedBlock(Integer relX, Integer relY, Integer relZ, Material blockType, byte data) {
        this.relX = relX;
        this.relY = relY;
        this.relZ = relZ;
        this.blockType = blockType;
        this.data = data;
    }

    public Integer getRelX() {
        return relX;
    }

    public Integer getRelY() {
        return relY;
    }

    public Integer getRelZ() {
        return relZ;
    }

    public Material getBlockType() {
        return blockType;
    }

    public byte getData() {
        return data;
    }

    public void setData(byte data) {
        this.data = data;
    }

    public void setBlockType(Material blockType) {
        this.blockType = blockType;
    }

    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> serialized = new HashMap<>();
        serialized.put("x", relX);
        serialized.put("y", relY);
        serialized.put("z", relZ);
        serialized.put("blockType", blockType.name());
        serialized.put("data", data);
        return serialized;
    }

    public static ChangedBlock deserialize(Map<String, Object> serialized){

        return new ChangedBlock(
                (Integer)serialized.get("x"),
                (Integer)serialized.get("y"),
                (Integer)serialized.get("z"),
                Material.valueOf((String)serialized.get("blockType")),
                ((Number)serialized.get("data")).byteValue());
    }
}
