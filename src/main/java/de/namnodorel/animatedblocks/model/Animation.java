package de.namnodorel.animatedblocks.model;

import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Animation implements ConfigurationSerializable{

    private String name;
    private List<Frame> frames;

    public Animation(String name){
        this.name = name;
        frames = new ArrayList<>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addFrame(Frame frame){
        frames.add(frame);
    }

    public List<Frame> getFrames() {
        return frames;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Animation)) return false;

        Animation animation = (Animation) o;

        if (!getName().equals(animation.getName())) return false;
        return getFrames().equals(animation.getFrames());
    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + getFrames().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Animation{" +
                "name='" + name + '\'' +
                ", frames=" + frames +
                '}';
    }

    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> serialized = new HashMap<>();
        serialized.put("name", name);
        serialized.put("frames", frames);
        return serialized;
    }

    public static Animation deserialize(Map<String, Object> seriaized){
        Animation deserialized = new Animation((String)seriaized.get("name"));
        deserialized.frames = (List<Frame>)seriaized.get("frames");
        return deserialized;
    }
}
