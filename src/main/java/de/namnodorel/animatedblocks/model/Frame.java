package de.namnodorel.animatedblocks.model;

import de.namnodorel.animatedblocks.actions.Action;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.*;

public class Frame implements ConfigurationSerializable{

    private HashSet<ChangedBlock> changedBlocks = new HashSet<>();
    private List<Action> actions = new ArrayList<>();
    private Double timeUntilNext;

    public Frame(Double timeUntilNext) {
        this.timeUntilNext = timeUntilNext;
    }

    public void addAction(Action action){
        actions.add(action);
    }

    public void runActions(Location absolutePosition){
        for(Action action : actions){
            action.run(absolutePosition.clone());
        }
    }

    public void changeBlock(Integer x, Integer y, Integer z, Material mat, byte data){
        ChangedBlock b = getChangedBlockFor(x, y, z);
        if(b != null){
            b.setBlockType(mat);
            b.setData(data);
        }else{
            changedBlocks.add(new ChangedBlock(x, y, z, mat, data));
        }
    }

    public void removeChange(Integer x, Integer y, Integer z){
        if(getChangedBlockFor(x, y, z) != null){
            changedBlocks.remove(getChangedBlockFor(x, y, z));
        }
    }

    private ChangedBlock getChangedBlockFor(Integer x, Integer y, Integer z){
        for(ChangedBlock block : changedBlocks){
            if(block.getRelX().equals(x)
                    && block.getRelY().equals(y)
                    && block.getRelZ().equals(z)){
                return block;
            }
        }
        return null;
    }

    public void setTimeUntilNext(Double timeUntilNext) {
        this.timeUntilNext = timeUntilNext;
    }

    public HashSet<ChangedBlock> getChangedBlocks() {
        return changedBlocks;
    }

    public Double getTimeUntilNext() {
        return timeUntilNext;
    }

    public List<Action> getActions() {
        return actions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Frame)) return false;

        Frame frame = (Frame) o;

        if (!getChangedBlocks().equals(frame.getChangedBlocks())) return false;
        if (!getActions().equals(frame.getActions())) return false;
        return getTimeUntilNext().equals(frame.getTimeUntilNext());
    }

    @Override
    public int hashCode() {
        int result = getChangedBlocks().hashCode();
        result = 31 * result + getActions().hashCode();
        result = 31 * result + getTimeUntilNext().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Frame{" +
                "changedBlocks=" + changedBlocks +
                ", actions=" + actions +
                ", timeUntilNext=" + timeUntilNext +
                '}';
    }

    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> serialized = new HashMap<>();

        serialized.put("changedBlocks", changedBlocks);
        serialized.put("actions", actions);
        serialized.put("time", timeUntilNext);

        return serialized;
    }

    public static Frame deserialize(Map<String, Object> serialized){
        Frame deserialized = new Frame((Double)serialized.get("time"));
        deserialized.changedBlocks = (HashSet<ChangedBlock>)serialized.get("changedBlocks");
        deserialized.actions = (List<Action>)serialized.get("actions");
        return deserialized;
    }
}
