package de.namnodorel.animatedblocks;

import de.namnodorel.animatedblocks.actions.Action;
import de.namnodorel.animatedblocks.model.Animation;
import de.namnodorel.animatedblocks.model.Frame;
import de.namnodorel.animatedblocks.player.AnimationContainer;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public abstract class DataHolder {

    private static CopyOnWriteArraySet<AnimationContainer> containers;
    private static CopyOnWriteArraySet<Animation> animations;

    private static HashMap<String, Animation> selectedAnimations;
    private static HashMap<String, Frame> selectedFrames;
    private static HashMap<String, Action> selectedActions;
    private static HashMap<String, AnimationContainer> selectedContainers;
    public static HashMap<String, Location> referenceLocations;

    private DataHolder(){

    }

    public static Animation getAnimation(String name){
        for(Animation a : animations){
            if(a.getName().equalsIgnoreCase(name)){
                return a;
            }
        }
        throw new IllegalArgumentException("There exists no animation with the given name " + name);
    }

    public static boolean animationExists(String name){
        for(Animation a : animations){
            if(a.getName().equalsIgnoreCase(name)){
                return true;
            }
        }
        return false;
    }

    public static AnimationContainer getContainer(String name){
        for(AnimationContainer ac : containers){
            if(ac.getName().equalsIgnoreCase(name)){
                return ac;
            }
        }
        throw new IllegalArgumentException("There exists no container with the given name " + name);
    }

    public static boolean containerExists(String name){
        for(AnimationContainer ac : containers){
            if(ac.getName().equalsIgnoreCase(name)){
                return true;
            }
        }
        return false;
    }

    public static void addContainer(AnimationContainer container){
        if(!containers.contains(container)){
            containers.add(container);
        }else{
            throw new IllegalArgumentException("The given container already exists: " + container.toString());
        }
    }

    public static void addAnimation(Animation animation){
        if(!animations.contains(animation)){
            animations.add(animation);
        }else{
            throw new IllegalArgumentException("The given animation already exists: " + animation.toString());
        }
    }

    public static CopyOnWriteArraySet<AnimationContainer> getContainers() {
        return containers;
    }

    public static CopyOnWriteArraySet<Animation> getAnimations() {
        return animations;
    }

    public static void selectAnimation(String playerName, Animation a){

        if(a == null){
            selectedAnimations.remove(playerName);
            return;
        }

        if(!animations.contains(a)){
            throw new IllegalArgumentException("Animation not found, can't select it: " + a.toString());
        }

        selectedAnimations.put(playerName, a);
    }

    public static void selectFrame(String playerName, Integer index){

        if(index == null){
            selectedFrames.remove(playerName);
            return;
        }

        Animation selectedAnimation = getSelectedAnimation(playerName);
        if(selectedAnimation == null){
            throw new IllegalArgumentException("There is no animation selected, can't select a frame!");
        }

        selectedFrames.put(playerName, selectedAnimation.getFrames().get(index));
    }

    public static void selectAction(String playerName, Integer index){

        if(index == null){
            selectedActions.remove(playerName);
            return;
        }

        Frame selectedFrame = getSelectedFrame(playerName);
        if(selectedFrame == null){
            throw new IllegalArgumentException("There is no frame selected, can't select an action!");
        }

        selectedActions.put(playerName, selectedFrame.getActions().get(index));
    }

    public static void selectContainer(String playerName, AnimationContainer ac){

        if(ac == null){
            selectedContainers.remove(playerName);
            return;
        }

        if(!containers.contains(ac)){
            throw new IllegalArgumentException("Container not found, can't select it: " + ac.toString());
        }

        selectedContainers.put(playerName, ac);
    }

    public static Animation getSelectedAnimation(String playerName){
        return selectedAnimations.get(playerName);
    }

    public static Frame getSelectedFrame(String playerName){
        return selectedFrames.get(playerName);
    }

    public static Action getSelectedAction(String playerName){
        return selectedActions.get(playerName);
    }

    public static AnimationContainer getSelectedContainer(String playerName){
        return selectedContainers.get(playerName);
    }

    public static void setReferenceLocation(String playerName, Location loc){
        if(loc == null){
            referenceLocations.remove(playerName);
            return;
        }

        loc.setX(Math.floor(loc.getX()));
        loc.setY(Math.floor(loc.getY()));
        loc.setZ(Math.floor(loc.getZ()));

        referenceLocations.put(playerName, loc);
    }

    public static Location getReferenceLocation(String playerName){
        return referenceLocations.get(playerName);
    }

    public static void removeAnimation(String animationName){
        animations.remove(getAnimation(animationName));
    }

    public static void removeContainer(String containerName){
        containers.remove(getContainer(containerName));
    }

    public static void reload(){

        selectedAnimations = new HashMap<>();
        selectedFrames = new HashMap<>();
        selectedActions = new HashMap<>();
        selectedContainers = new HashMap<>();
        referenceLocations = new HashMap<>();

        FileConfiguration animationData = YamlConfiguration.loadConfiguration(new File(AnimatedBlocks.getInstance().getDataFolder(), "animations.yml"));

        Set<Animation> tmpAnimations = (Set<Animation>) animationData.get("animations");
        animations = new CopyOnWriteArraySet<>();
        if(tmpAnimations != null){
            animations.addAll(tmpAnimations);
        }

        FileConfiguration containerData = YamlConfiguration.loadConfiguration(new File(AnimatedBlocks.getInstance().getDataFolder(), "containers.yml"));
        Set<AnimationContainer> tmpContainers = (Set<AnimationContainer>) containerData.get("containers");
        containers = new CopyOnWriteArraySet<>();
        if(tmpContainers != null){
            containers.addAll(tmpContainers);
        }
    }

    public static void save(){
        FileConfiguration animationData = YamlConfiguration.loadConfiguration(new File(AnimatedBlocks.getInstance().getDataFolder(), "animations.yml"));
        animationData.set("animations", animations);
        try {
            animationData.save(new File(AnimatedBlocks.getInstance().getDataFolder(), "animations.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileConfiguration containerData = YamlConfiguration.loadConfiguration(new File(AnimatedBlocks.getInstance().getDataFolder(), "containers.yml"));
        containerData.set("containers", containers);
        try {
            containerData.save(new File(AnimatedBlocks.getInstance().getDataFolder(), "containers.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
