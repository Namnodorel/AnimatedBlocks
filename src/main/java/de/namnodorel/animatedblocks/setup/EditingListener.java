package de.namnodorel.animatedblocks.setup;

import de.namnodorel.animatedblocks.DataHolder;
import de.namnodorel.animatedblocks.model.Frame;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class EditingListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBlockBreak(BlockBreakEvent event){

        if(DataHolder.getReferenceLocation(event.getPlayer().getName()) != null){

            if(!event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.BLAZE_ROD)){
                Frame f = DataHolder.getSelectedFrame(event.getPlayer().getName());
                Location l = DataHolder.getReferenceLocation(event.getPlayer().getName()).clone();
                l.subtract(event.getBlock().getX(), event.getBlock().getY(), event.getBlock().getZ());
                f.changeBlock((int)Math.floor(l.getX()), (int)Math.floor(l.getY()), (int)Math.floor(l.getZ()), Material.AIR, (byte)0);
            }

        }

    }


    @EventHandler (priority = EventPriority.MONITOR)
    public void onBlockPlace(BlockPlaceEvent event){

        if(DataHolder.getReferenceLocation(event.getPlayer().getName()) != null){
            Frame f = DataHolder.getSelectedFrame(event.getPlayer().getName());
            Location l = DataHolder.getReferenceLocation(event.getPlayer().getName()).clone();
            l.subtract(event.getBlock().getX(), event.getBlock().getY(), event.getBlock().getZ());
            f.changeBlock((int)Math.floor(l.getX()), (int)Math.floor(l.getY()), (int)Math.floor(l.getZ()), event.getBlock().getType(), event.getBlock().getData());
        }

    }

    @EventHandler (priority = EventPriority.HIGH)
    public void onRightClick(PlayerInteractEvent event){
        if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
            if(DataHolder.getReferenceLocation(event.getPlayer().getName()) != null){
                if(event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.BLAZE_ROD)){
                    Frame f = DataHolder.getSelectedFrame(event.getPlayer().getName());
                    Location l = DataHolder.getReferenceLocation(event.getPlayer().getName()).clone();
                    l.subtract(event.getClickedBlock().getX(), event.getClickedBlock().getY(), event.getClickedBlock().getZ());
                    f.removeChange((int)Math.floor(l.getX()), (int)Math.floor(l.getY()), (int)Math.floor(l.getZ()));                }
            }
        }

    }
}
