package de.namnodorel.animatedblocks.setup;

import de.namnodorel.animatedblocks.DataHolder;
import de.namnodorel.animatedblocks.actions.Action;
import de.namnodorel.animatedblocks.actions.SoundAction;
import de.namnodorel.animatedblocks.model.Animation;
import de.namnodorel.animatedblocks.model.ChangedBlock;
import de.namnodorel.animatedblocks.model.Frame;
import de.namnodorel.animatedblocks.player.AnimationContainer;
import de.namnodorel.animatedblocks.player.LoopMode;
import de.namnodorel.animatedblocks.utils.NumberUtil;
import de.namnodorel.messageutils.MessageBlock;
import de.namnodorel.messageutils.message.FancyMessage;
import org.apache.commons.lang3.EnumUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

//TODO Command action

public class AnimatedBlocksCommand implements CommandExecutor{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(!sender.isOp()){
            sender.sendMessage("§cDu hast nicht die notwendige Berechtigung, um diesen Befehl zu nutzen!");
            return true;
        }

        if(args.length == 0){
            sender.sendMessage("§c/animatedblocks <create/list/edit/delete/select/play/pause/stop>");
            return true;
        }

        if(args[0].equalsIgnoreCase("create")){

            if(args.length >= 2){

                if(args[1].equalsIgnoreCase("animation")){

                    if(args.length == 3){

                        String name = args[2];
                        if(!DataHolder.animationExists(name)){

                            Animation a = new Animation(name);
                            DataHolder.addAnimation(a);
                            DataHolder.selectAnimation(sender.getName(), a);

                            sender.sendMessage("§aDie Animation " + name + " wurde erstellt und ausgewählt!");
                            sender.sendMessage("§aBenutze §e/animatedblocks create frame [time]§a , um neue Frames hinzuzufügen!");
                        }else{
                            sender.sendMessage("§cDie Animation " + name + " existiert bereits!");
                        }
                    }else{
                        sender.sendMessage("§c/animatedblocks create animation <name>");
                        sender.sendMessage("§eBitte beachte dabei, dass deine aktuelle Position dabei als relativer Anker verwendet wird!");
                    }

                }else if(args[1].equalsIgnoreCase("frame")){

                    if(sender instanceof Player){

                        Animation selectedAnimation = DataHolder.getSelectedAnimation(sender.getName());
                        if(selectedAnimation != null){

                            Double time = 1.0;
                            if(args.length == 3){
                                if(NumberUtil.isDouble(args[2])){
                                    time = Double.valueOf(args[2]);
                                }else{
                                    sender.sendMessage("§c" + args[2] + " wurde nicht als gültige Kommazahl erkannt, benutze den Standartwert 1.0...");
                                }
                            }

                            if(DataHolder.getReferenceLocation(sender.getName()) != null){
                                for(ChangedBlock b : DataHolder.getSelectedFrame(sender.getName()).getChangedBlocks()) {
                                    Bukkit.getPlayer(sender.getName()).sendBlockChange(DataHolder.getReferenceLocation(sender.getName()).clone().subtract(b.getRelX(), b.getRelY(), b.getRelZ()), b.getBlockType(), (byte) b.getData());
                                }
                            }

                            Frame f = new Frame(time);
                            selectedAnimation.addFrame(f);
                            DataHolder.selectFrame(sender.getName(), selectedAnimation.getFrames().indexOf(f));

                            sender.sendMessage("§aNeuer Frame wurde erstellt und ausgewählt!");

                            if(DataHolder.getReferenceLocation(sender.getName()) == null){
                                DataHolder.setReferenceLocation(sender.getName(), ((Player) sender).getLocation());
                                sender.sendMessage("§aAusserdem wurdest du in den Edit-Modus für diesen Frame gesetzt; alle Blöcke, die du jetzt abbaust");
                                sender.sendMessage("§awerden jetzt als Änderung zu diesem Frame hinzugefügt! Deine aktuelle Position dient dabei als Ankerpunkt.");
                            }

                        }else{
                            sender.sendMessage("§cDu musst zuerst eine Animation auswählen, bevor du Frames zu ihr hinzufügen kannst!");
                        }

                    }else{
                        sender.sendMessage("§cDieser Befehl ist nur für Spieler!");
                    }

                }else if(args[1].equalsIgnoreCase("action")){

                    if(args.length >= 3){

                        Frame selectedFrame = DataHolder.getSelectedFrame(sender.getName());
                        if(selectedFrame != null){

                            String action = args[2];
                            if(action.equalsIgnoreCase("sound")){

                                if(sender instanceof Player){

                                    if(args.length == 13){

                                        Double sX;
                                        if(NumberUtil.isDouble(args[3])){

                                            sX = Double.valueOf(args[3]);

                                            Double sY;
                                            if(NumberUtil.isDouble(args[4])){
                                                sY= Double.valueOf(args[4]);

                                                Double sZ;
                                                if(NumberUtil.isDouble(args[5])){
                                                    sZ = Double.valueOf(args[5]);

                                                    Integer radius;
                                                    if(NumberUtil.isInteger(args[6])){
                                                        radius = Integer.valueOf(args[6]);

                                                        Sound sound;
                                                        if(EnumUtils.isValidEnum(Sound.class, args[7])){
                                                            sound = Sound.valueOf(args[7]);

                                                            Double x;
                                                            if(NumberUtil.isDouble(args[8])){
                                                                x = Double.valueOf(args[8]);

                                                                Double y;
                                                                if(NumberUtil.isDouble(args[9])){
                                                                    y = Double.valueOf(args[9]);

                                                                    Double z;
                                                                    if(NumberUtil.isDouble(args[10])){
                                                                        z = Double.valueOf(args[10]);

                                                                        Float pitch;
                                                                        if(NumberUtil.isDouble(args[11])){
                                                                            pitch = Double.valueOf(args[11]).floatValue();

                                                                            Float volume;
                                                                            if(NumberUtil.isDouble(args[12])){
                                                                                volume = Double.valueOf(args[12]).floatValue();

                                                                                selectedFrame.addAction(new SoundAction(sX, sY, sZ, radius, sound, x, y, z, pitch, volume));
                                                                                DataHolder.selectAction(sender.getName(), selectedFrame.getActions().size() - 1);

                                                                                sender.sendMessage("§aDie neue Aktion wurde erstellt und ausgewählt!");
                                                                            }else{
                                                                                sender.sendMessage("§cBitte gib Volume/Pitch als Kommawert ein!");
                                                                            }
                                                                        }else{
                                                                            sender.sendMessage("§cBitte gib Volume/Pitch als Kommawert ein!");
                                                                        }
                                                                    }else{
                                                                        sender.sendMessage("§cBitte gib die Koordinaten als einfache Zahlen an!");
                                                                    }
                                                                }else{
                                                                    sender.sendMessage("§cBitte gib die Koordinaten als einfache Zahlen an!");
                                                                }
                                                            }else{
                                                                sender.sendMessage("§cBitte gib die als Koordinaten eine einfache Zahlen an!");
                                                            }
                                                        }else{
                                                            sender.sendMessage("§cBitte wähle einen Sound aus der Liste auf §ehttps://hub.spigotmc.org/javadocs/spigot/org/bukkit/Sound.html");
                                                        }
                                                    }else{
                                                        sender.sendMessage("§cBitte gib den Radius als einfache Zahl an!");
                                                    }
                                                }else{
                                                    sender.sendMessage("§cBitte gib die Koordinaten als einfache Zahlen an!");
                                                }
                                            }else{
                                                sender.sendMessage("§cBitte gib die Koordinaten als einfache Zahlen an!");
                                            }

                                        }else{
                                            sender.sendMessage("§cBitte gib die Koordinaten als einfache Zahlen an!");
                                        }

                                    }else{
                                        sender.sendMessage("§c/animatedblocks create action sound <sx> <sy> <sz> <radius> <sound> <x> <y> <z> <pitch> <volume>");
                                    }

                                }else{
                                    sender.sendMessage("§cDieser Befehl ist nur für Spieler");
                                }

                            }else{
                                sender.sendMessage("§cAktion wurde nicht gefunden!");
                            }

                        }else{
                            sender.sendMessage("§cDu musst zuerst einen Frame auswählen, bevor du eine Aktion hinzufügen kannst!");
                        }

                    }else{
                        sender.sendMessage("§c/animatedblocks create action <action> <...>");
                    }


                }else if(args[1].equalsIgnoreCase("container")){

                    if(args.length == 4){

                        if(sender instanceof Player){

                            String name = args[2];
                            if(!DataHolder.containerExists(name)){

                                String animationName = args[3];
                                if(DataHolder.animationExists(animationName)){

                                    Location loc = ((Player) sender).getLocation().clone();
                                    AnimationContainer container = new AnimationContainer(name, DataHolder.getAnimation(animationName), loc);
                                    DataHolder.addContainer(container);
                                    DataHolder.selectContainer(sender.getName(), container);
                                    sender.sendMessage("§aDer neue Container wurde erstellt und ausgewählt!");

                                }else{
                                    sender.sendMessage("§cEs wurde keine Animation mit diesem Namen gefunden!");
                                }

                            }else{
                                sender.sendMessage("§cEs gibt bereits einen Container mit diesem Namen!");
                            }

                        }else{
                            sender.sendMessage("§cDieser Befehl ist nur für Spieler!");
                        }

                    }else{
                        sender.sendMessage("§c/animatedblocks create container <name> <animation>");
                        sender.sendMessage("§eDeine aktuelle Position wird dabei als Anker für die Animation verwendet.");
                    }

                }else{
                    sender.sendMessage("§c/animatedblocks create <animation/frame/action/container>");
                }

            }else{
                sender.sendMessage("§c/animatedblocks create <animation/frame/action/container>");
            }

        }else if(args[0].equalsIgnoreCase("list")){

            if(args.length == 2){

                if(args[1].equalsIgnoreCase("animations")){

                    if(DataHolder.getAnimations().size() > 0){

                        if(sender instanceof Player){

                            MessageBlock msgBlock = new MessageBlock(6);

                            for(Animation a : DataHolder.getAnimations()){
                                msgBlock.addMessage(new FancyMessage("§a" + a.getName() + " §e[" + a.getFrames().size() + " Frames]")
                                    .command("/animatedblocks select animation " + a.getName()));
                            }

                            MessageBlock.sendMessagesToPlayer((Player) sender, msgBlock);

                        }else{
                            for(Animation a : DataHolder.getAnimations()){
                                sender.sendMessage("§a" + a.getName() + " §e[" + a.getFrames().size() + " Frames]");
                            }
                        }

                    }else{
                        sender.sendMessage("§cKeine Animationen gefunden.");
                    }

                }else if(args[1].equalsIgnoreCase("frames")){

                    Animation selectedAnimation = DataHolder.getSelectedAnimation(sender.getName());

                    if(selectedAnimation != null){

                        if(selectedAnimation.getFrames().size() > 0){

                            if(sender instanceof Player){

                                MessageBlock msgBlock = new MessageBlock(6);

                                for(Frame f : selectedAnimation.getFrames()){
                                    msgBlock.addMessage(new FancyMessage("§a" + selectedAnimation.getFrames().indexOf(f) + " §e" + f.getChangedBlocks().size() + " geänderte Blöcke; " + f.getActions().size() + " Aktionen; Dauer: " + f.getTimeUntilNext())
                                        .command("/animatedblocks select frame " + selectedAnimation.getFrames().indexOf(f)));
                                }

                                MessageBlock.sendMessagesToPlayer((Player) sender, msgBlock);
                            }else{
                                for(Frame f : selectedAnimation.getFrames()){
                                    sender.sendMessage("§a" + selectedAnimation.getFrames().indexOf(f) + " §e" + f.getChangedBlocks() + " geänderte Blöcke; " + f.getActions().size() + " Aktionen; Dauer: " + f.getTimeUntilNext());
                                }
                            }

                        }else{
                            sender.sendMessage("§cKeine Frames gefunden.");
                        }

                    }else{
                        sender.sendMessage("§cDu musst zuerst eine Animation ausgewählt haben, bevor du ihre Frames auflisten kannst!");
                    }

                }else if(args[1].equalsIgnoreCase("actions")){

                    Frame selectedFrame = DataHolder.getSelectedFrame(sender.getName());
                    if(selectedFrame != null){

                        if(selectedFrame.getActions().size() > 0){

                            if(sender instanceof Player){
                                MessageBlock msgBlock = new MessageBlock(1);

                                for(Action a : selectedFrame.getActions()){
                                    if(a instanceof SoundAction){
                                        msgBlock.addMessage(new FancyMessage("").then(
                                                "§e" + selectedFrame.getActions().indexOf(a) + "§a " + ((SoundAction) a).getSound() +
                                                        "\n§e sX:§a" + ((SoundAction) a).getSelectorX() +
                                                        "\n§e sY:§a" + ((SoundAction) a).getSelectorY() +
                                                        "\n§e sZ:§a" + ((SoundAction) a).getSelectorZ() +
                                                        "\n§e radius:§a" + ((SoundAction) a).getRadius()+
                                                        "\n§e x:§a" + ((SoundAction) a).getX() +
                                                        "\n§e y:§a" + ((SoundAction) a).getY() +
                                                        "\n§e z:§a" + ((SoundAction) a).getZ() +
                                                        "\n§e Pitch:§a" + ((SoundAction) a).getPitch() +
                                                        "\n§e Volume:§a" + ((SoundAction) a).getVolume())
                                        .command("/ab select action " + selectedFrame.getActions().indexOf(a)));
                                    }else{
                                        sender.sendMessage("§cUnbekannte Aktion: " + a);
                                    }
                                }
                                MessageBlock.sendMessagesToPlayer((Player) sender, msgBlock);
                            }else{
                                for(Action a : selectedFrame.getActions()){
                                    if(a instanceof SoundAction){
                                        sender.sendMessage("§e" + selectedFrame.getActions().indexOf(a) + "§a " + ((SoundAction) a).getSound() +
                                                "\n§e sX:§a" + ((SoundAction) a).getSelectorX() +
                                                "\n§e sY:§a" + ((SoundAction) a).getSelectorY() +
                                                "\n§e sZ:§a" + ((SoundAction) a).getSelectorZ() +
                                                "\n§e radius:§a" + ((SoundAction) a).getRadius()+
                                                "\n§e x:§a" + ((SoundAction) a).getX() +
                                                "\n§e y:§a" + ((SoundAction) a).getY() +
                                                "\n§e z:§a" + ((SoundAction) a).getZ() +
                                                "\n§e Pitch:§a" + ((SoundAction) a).getPitch() +
                                                "\n§e Volume:§a" + ((SoundAction) a).getVolume());
                                    }
                                }
                            }

                        }else{
                            sender.sendMessage("§cKeine Aktionen gefunden.");
                        }

                    }else{
                        sender.sendMessage("§cDu musst zuerst einen Frame ausgewählt haben, bevor du seine Aktionen auflisten kannst!");
                    }

                }else if(args[1].equalsIgnoreCase("containers")){

                    if(DataHolder.getContainers().size() > 0){

                        if(sender instanceof Player){

                            MessageBlock msgBlock = new MessageBlock(1);
                            for(AnimationContainer ac : DataHolder.getContainers()){
                                FancyMessage msg = new FancyMessage("§a" + ac.getName());
                                msg.then("\n§aAnimation:§e " + ac.getAnimation().getName());
                                msg.then("\n§aPosition:§e X:" + ac.getAbsolutePosition().getX() + " Y:" + ac.getAbsolutePosition().getY() + " Z:" + ac.getAbsolutePosition().getZ());
                                msg.then("\n§aAutoplay:§e" + ac.getAutoplay());
                                msg.then("\n§aLoopModus:§e " + ac.getLoopMode());
                                if(ac.getLoopMode().equals(LoopMode.WAIT_RANDOM)){
                                    msg.then("\n§aRandomBounds: [Min:§c" + ac.getRandomWaitMin() + "§e][Max:§c" + ac.getRandomWaitMax() + "§e]");
                                }
                                if(ac.getOffset() != -1){
                                    msg.then("\n§aOffset:§e " + ac.getOffset());
                                }
                                if(ac.getStartFrame() != -1 || ac.getStopFrame() != -1){
                                    msg.then("\n§aBounds:§e " + (ac.getStartFrame() != -1?("[Start:§c" + ac.getStartFrame()) + "§e]":"") + (ac.getStopFrame() != -1?("[Stop:§c" + ac.getStopFrame()) + "§e]":""));
                                }
                            }
                            MessageBlock.sendMessagesToPlayer((Player) sender, msgBlock);
                        }else{
                            for(AnimationContainer ac : DataHolder.getContainers()){
                                sender.sendMessage("§a" + ac.getName());
                                sender.sendMessage("\t§aAnimation:§e " + ac.getAnimation().getName());
                                sender.sendMessage("\t§aPosition:§e X:" + ac.getAbsolutePosition().getX() + " Y:" + ac.getAbsolutePosition().getY() + " Z:" + ac.getAbsolutePosition().getZ());
                                sender.sendMessage("\t§aAutoplay:§e" + ac.getAutoplay());
                                sender.sendMessage("\t§aLoopModus:§e " + ac.getLoopMode());
                                if(ac.getLoopMode().equals(LoopMode.WAIT_RANDOM)){
                                    sender.sendMessage("\t§aRandomBounds: [Min:§c" + ac.getRandomWaitMin() + "§e][Max:§c" + ac.getRandomWaitMax() + "§e]");
                                }
                                sender.sendMessage("\t§aOffset:§e " + ac.getOffset());
                                sender.sendMessage("\t§aBounds:§e " + (ac.getStartFrame() != -1?("[Start:§c" + ac.getStartFrame()) + "§e]":"") + (ac.getStopFrame() != -1?("[Stop:§c" + ac.getStopFrame()) + "§e]":""));

                            }
                        }

                    }else{
                        sender.sendMessage("§cKeine Container gefunden.");
                    }

                }else{
                    sender.sendMessage("§c/animatedblocks list <animations/frames/actions/containers>");
                }

            }else{
                sender.sendMessage("§c/animatedblocks list <animations/frames/actions/containers>");
            }

        }else if(args[0].equalsIgnoreCase("edit")){

            if(args.length >= 2){

                if(args[1].equalsIgnoreCase("animation")){

                    if(args.length != 4 || !args[2].equalsIgnoreCase("name")){
                        sender.sendMessage("§c/animatedblocks edit animation name <name>");
                    }else{
                        Animation selectedAnimation = DataHolder.getSelectedAnimation(sender.getName());

                        if(selectedAnimation != null){
                            String name = args[3];
                            if(!DataHolder.animationExists(name)){
                                DataHolder.removeAnimation(selectedAnimation.getName());
                                selectedAnimation.setName(name);
                                DataHolder.addAnimation(selectedAnimation);
                                DataHolder.selectAnimation(sender.getName(), selectedAnimation);

                                sender.sendMessage("§aDie Animation wurde umbenannt in §e" + name + "§a!");
                            }else{
                                sender.sendMessage("§cEs gibt bereits eine Animation mit dem Namen " + name + "!");
                            }
                        }else{
                            sender.sendMessage("§cDu musst zuerst eine Animation auswählen, bevor du ihren Namen ändern kannst!");
                        }
                    }

                }else if(args[1].equalsIgnoreCase("frame")){

                    if(args.length >= 3){

                        Frame selectedFrame = DataHolder.getSelectedFrame(sender.getName());
                        if(selectedFrame != null){
                            if(args[2].equalsIgnoreCase("blocks")){

                                if(sender instanceof Player){

                                    if(DataHolder.getReferenceLocation(sender.getName()) != null){
                                        for(ChangedBlock b : DataHolder.getSelectedFrame(sender.getName()).getChangedBlocks()) {
                                            Bukkit.getPlayer(sender.getName()).sendBlockChange(DataHolder.getReferenceLocation(sender.getName()).clone().subtract(b.getRelX(), b.getRelY(), b.getRelZ()), b.getBlockType(), (byte) b.getData());
                                        }
                                    }

                                    DataHolder.setReferenceLocation(sender.getName(), ((Player) sender).getLocation());

                                    sender.sendMessage("§aEditModus wurde geöffnet mit deiner aktuellen Position als Referenzpunkt!");

                                }else{
                                    sender.sendMessage("§cDieser Befehl ist nur für Spieler!");
                                }

                            }else if(args[2].equalsIgnoreCase("time")){

                                if(args.length == 4){

                                    if(NumberUtil.isDouble(args[3])){
                                        selectedFrame.setTimeUntilNext(Double.valueOf(args[3]));
                                        sender.sendMessage("§aZeitabstand wurde auf §e" + args[3] + "§a gesetzt!");
                                    }else{
                                        sender.sendMessage("§cBitte gib eine gültige Zahl an! (Seperator ist ein Punkt, kein Komma)");
                                    }

                                }else{
                                    sender.sendMessage("§c/animatedblocks edit frame time <time>");
                                }

                            }else{
                                sender.sendMessage("§c/animatedblocks edit frame <blocks/time>");
                            }
                        }else{
                            sender.sendMessage("§cDu musst zuerst einen Frame ausgewählt haben, bevor du ihn verändern kannst!");
                        }

                    }else{
                        sender.sendMessage("$c/animatedblocks edit frame <blocks/time>");
                    }

                }else if(args[1].equalsIgnoreCase("action")){

                    if(args.length >= 3){

                        Action selectedAction = DataHolder.getSelectedAction(sender.getName());
                        if(selectedAction != null){
                            //TODO Implement editing of all aspects of an action
                        }else{
                            sender.sendMessage("§cDu musst zuerst eine Aktion auswählen, bevor du sie verändern kannst!");
                        }

                    }else{
                        sender.sendMessage("§c/animatedblocks edit action <action>");
                    }

                }else if(args[1].equalsIgnoreCase("container")){

                    AnimationContainer selectedContainer = DataHolder.getSelectedContainer(sender.getName());
                    if(selectedContainer != null){
                        if(args.length >= 3){

                            if(args[2].equalsIgnoreCase("name")){

                                if(args.length == 4){

                                    String name = args[3];
                                    if(!DataHolder.containerExists(name)){

                                        selectedContainer.setName(name);
                                        sender.sendMessage("§aDer Container wurde in §e" + name + "§a umbenannt!");

                                    }else{
                                        sender.sendMessage("§cEs gibt bereits einen Container mit dem Namen " + name + "!");
                                    }

                                }else{
                                    sender.sendMessage("§c/animatedblocks edit container name <name>");
                                }

                            }else if(args[2].equalsIgnoreCase("animation")){

                                if(args.length == 4){

                                    if(DataHolder.animationExists(args[3])){

                                        selectedContainer.setAnimation(DataHolder.getAnimation(args[3]));
                                        sender.sendMessage("§aDer Container bezieht sich nun auf die Animation §e" + args[3] + "§a!");

                                    }else{
                                        sender.sendMessage("§cEs wurde keine Animation mit diesem Namen gefunden!");
                                    }

                                }else{
                                    sender.sendMessage("§c/animatedblocks edit container animation <animation>");
                                }

                            }else if(args[2].equalsIgnoreCase("position")){

                                if(sender instanceof Player){

                                    selectedContainer.setAbsolutePosition(((Player) sender).getLocation().clone());
                                    sender.sendMessage("§aDer Ankerpunkt des Containers wurde zu deiner aktuellen Position gesetzt!");

                                }else{
                                    sender.sendMessage("§cDieser Befehl ist nur für Spieler!");
                                }

                            }else if(args[2].equalsIgnoreCase("loop")){

                                if(args.length >= 4){

                                    if(args[3].equalsIgnoreCase("none")){
                                        selectedContainer.setLoopMode(LoopMode.NONE);
                                        selectedContainer.setRandomWaitMin(-1);
                                        selectedContainer.setRandomWaitMax(-1);

                                        sender.sendMessage("§aDer LoopModus des Containers wurde auf NONE gesetzt!");
                                    }else if(args[3].equalsIgnoreCase("instant")){
                                        selectedContainer.setLoopMode(LoopMode.CONTINUE_INSTANTLY);
                                        selectedContainer.setRandomWaitMin(-1);
                                        selectedContainer.setRandomWaitMax(-1);

                                        sender.sendMessage("§aDer LoopModus des Containers wurde auf CONTINUE_INSTANTLY gesetzt!");
                                    }else if(args[3].equalsIgnoreCase("offset")){

                                        if(selectedContainer.getOffset() != -1){
                                            selectedContainer.setLoopMode(LoopMode.WAIT_FOR_OFFSET);
                                            selectedContainer.setRandomWaitMin(-1);
                                            selectedContainer.setRandomWaitMax(-1);

                                            sender.sendMessage("§aDer LoopModus des Containers wurde auf WAIT_FOR_OFFSET gesetzt!");
                                        }else{
                                            sender.sendMessage("§cBitte wähle zuerst einen Wert für Offset!");
                                        }
                                    }else if(args[3].equalsIgnoreCase("random")){
                                        if(args.length == 6){

                                            if(NumberUtil.isDouble(args[4]) && Double.valueOf(args[4]) >= 0){
                                                long randomWaitMin = (long)(Double.valueOf(args[4])*20);

                                                if(NumberUtil.isDouble(args[5]) && Double.valueOf(args[5]) >= 0){
                                                    long randomWaitMax = (long)(Double.valueOf(args[5])*20);

                                                    selectedContainer.setLoopMode(LoopMode.WAIT_RANDOM);
                                                    selectedContainer.setRandomWaitMin(randomWaitMin);
                                                    selectedContainer.setRandomWaitMax(randomWaitMax);

                                                    sender.sendMessage("§aDer LoopModus des Containers wurde auf WAIT_RANDOM gesetzt!");
                                                }else{
                                                    sender.sendMessage("§cBitte gib entweder einen korrekten Zahlenwert ein! (Trennzeichen ist ein Punkt, kein Komma)");
                                                }
                                            }else{
                                                sender.sendMessage("§cBitte gib entweder einen korrekten Zahlenwert ein! (Trennzeichen ist ein Punkt, kein Komma)");
                                            }
                                        }else{
                                            sender.sendMessage("§c/animatedblocks edit container loop random <min> <max>");
                                        }
                                    }

                                }else{
                                    sender.sendMessage("§c/animatedblocks edit container loop <none/instant/offset/random>");
                                }

                            }else if(args[2].equalsIgnoreCase("offset")){

                                if(args.length == 4){

                                    if(NumberUtil.isInteger(args[3])){

                                        Integer parsedInt = Integer.valueOf(args[3]);
                                        if(parsedInt >= 1 && parsedInt <= 1200){
                                            selectedContainer.setOffset(parsedInt);

                                            sender.sendMessage("§aOffset wurde zu §e" + parsedInt + " §agesetzt!");
                                        }else{
                                            sender.sendMessage("§cBitte gib eine gültige natürliche Zahl an, die im Bereich 1-1200 (inklusive) liegt!");
                                        }

                                    }else{
                                        sender.sendMessage("§cBitte gib eine gültige natürliche Zahl an, die im Bereich 1-1200 (inklusive) liegt!");
                                    }

                                }else{
                                    sender.sendMessage("§c/animatedblocks edit container offset  <1-1200>");
                                }

                            }else if(args[2].equalsIgnoreCase("bonds")) {

                                if (args.length == 5) {

                                    Integer start = null;
                                    Integer stop = null;

                                    if (args[3].equalsIgnoreCase("none")) {
                                        start = -1;
                                    } else if (NumberUtil.isInteger(args[3])) {
                                        start = Integer.valueOf(args[3]);
                                    } else {
                                        sender.sendMessage("§cBitte gib eine gültige natürliche Zahl oder 'none' an!");
                                    }

                                    if (start != null) {
                                        if (args[4].equalsIgnoreCase("none")) {
                                            stop = -1;
                                        } else if (NumberUtil.isInteger(args[4])) {
                                            stop = Integer.valueOf(args[4]);
                                        } else {
                                            sender.sendMessage("§cBitte gib eine gültige natürliche Zahl oder 'none' an!");
                                        }

                                        if (stop != null) {

                                            selectedContainer.setStartFrame(start);
                                            selectedContainer.setStopFrame(stop);

                                            sender.sendMessage("§aDie neuen Start- und Stopframes wurden festgelegt!");
                                        }
                                    }

                                } else {
                                    sender.sendMessage("§c/animatedblocks edit container bonds <start> <stop>");
                                }
                            }else if(args[2].equalsIgnoreCase("autoplay")){

                                selectedContainer.setAutoplay(!selectedContainer.getAutoplay());
                                sender.sendMessage("§aAutoplay wurde gesetzt auf §e" + selectedContainer.getAutoplay());

                            }else{
                                sender.sendMessage("§c/animatedblocks edit container <name/animation/position/loop/offset/bonds>");
                            }

                        }else{
                            sender.sendMessage("§c/animatedblocks edit container <name/animation/position/loop/offset/bonds>");
                        }
                    }else{
                        sender.sendMessage("§cDu musst zuerst einen Container auswählen, bevor du ihn verändern kannst!");
                    }

                }else{
                    sender.sendMessage("§c/animatedblocks edit <animation/frame/action/container>");
                }

            }else{
                sender.sendMessage("§c/animatedblocks edit <animation/frame/action/container>");
            }

        }else if(args[0].equalsIgnoreCase("select")){

            if(args.length >= 2){

                if(args[1].equalsIgnoreCase("animation")){

                    if(args.length == 3){
                        String name = args[2];
                        if(DataHolder.animationExists(name)){

                            DataHolder.selectAnimation(sender.getName(), DataHolder.getAnimation(name));

                            sender.sendMessage("§aDie Animation §e" + name + "§a wurde ausgewählt!");
                        }else{
                            sender.sendMessage("§cEs wurde keine Animation mit diesem Namen gefunden!");
                        }
                    }else{
                        sender.sendMessage("§c/animatedblocks select animation <animation>");
                    }

                }else if(args[1].equalsIgnoreCase("frame")) {

                    Animation selectedAnimation = DataHolder.getSelectedAnimation(sender.getName());
                    if (selectedAnimation != null) {
                        if (selectedAnimation.getFrames().size() != 0) {
                            if (args.length == 3) {

                                if (NumberUtil.isInteger(args[2])) {

                                    Integer index = Integer.valueOf(args[2]);
                                    if (index >= 0 && index < selectedAnimation.getFrames().size()) {

                                        if(DataHolder.getReferenceLocation(sender.getName()) != null){
                                            for(ChangedBlock b : DataHolder.getSelectedFrame(sender.getName()).getChangedBlocks()) {
                                                Bukkit.getPlayer(sender.getName()).sendBlockChange(DataHolder.getReferenceLocation(sender.getName()).clone().subtract(b.getRelX(), b.getRelY(), b.getRelZ()), b.getBlockType(), (byte) b.getData());
                                            }
                                        }

                                        DataHolder.selectFrame(sender.getName(), index);

                                        sender.sendMessage("§aDer Frame im Index §e" + index + "§a wurde ausgewählt!");
                                    }else{
                                        sender.sendMessage("§cEs gibt keinen Frame mit dem Index " + index + "!");
                                    }

                                } else {
                                    sender.sendMessage("§cBitte gib eine gültige natürliche Zahl an!");
                                }

                            } else {
                                sender.sendMessage("§c/animatedblocks select frame <index>");
                            }
                        } else {
                            sender.sendMessage("§cDie ausgewählte Animation besitzt keine Frames. Wähle eine andere Animation oder erstelle neue Frames!");
                        }
                    } else {
                        sender.sendMessage("§cDu musst zuerst eine Animation auswählen, bevor du Frames in ihr auswählen kannst!");
                    }
                }else if(args[1].equalsIgnoreCase("action")){

                    Frame selectedFrame = DataHolder.getSelectedFrame(sender.getName());
                    if(selectedFrame != null){

                        if(selectedFrame.getActions().size() > 0){
                            if(args.length == 3){

                                if(NumberUtil.isInteger(args[2])){
                                    Integer index = Integer.valueOf(args[2]);

                                    if(index >= 0 && index < selectedFrame.getActions().size()){

                                        DataHolder.selectAction(sender.getName(), index);
                                        sender.sendMessage("§aDie Aktion im Index §e" + index + "§a wurde ausgewählt!");

                                    }else{
                                        sender.sendMessage("§cEs gibt keine Aktion mit dem Index " + index + "!");
                                    }
                                }else{
                                    sender.sendMessage("§cBitte gib eine gültige natürliche Zahl an!");
                                }

                            }else{
                                sender.sendMessage("§c/animatedblocks select action <index>");
                            }
                        }else{
                            sender.sendMessage("§cDer ausgewählte Frame besitzt keine Aktionen. Wähle einen anderen Frame oder erstelle neue Aktionen!");
                        }

                    }else{
                        sender.sendMessage("§cDu musst zuerst einen Frame auswählen, bevor du seine Aktionen auswählen kannst!");
                    }

                }else if(args[1].equalsIgnoreCase("container")){

                    if(args.length == 3){
                        String name = args[2];
                        if(DataHolder.containerExists(name)){

                            DataHolder.selectContainer(sender.getName(), DataHolder.getContainer(name));

                            sender.sendMessage("§aDer Container §e" + name + "§a wurde ausgewählt!");
                        }else{
                            sender.sendMessage("§cEs wurde kein Container mit diesem Namen gefunden!");
                        }
                    }else{
                        sender.sendMessage("§c/animatedblocks select container <container>");
                    }

                }else{
                    sender.sendMessage("§c/animatedblocks select <animation/frame/container>");
                }

            }else{
                sender.sendMessage("§c/animatedblocks select <animation/frame/action/container>");
            }

        }else if(args[0].equalsIgnoreCase("delete")){

            if(args.length == 2){

                if(args[1].equalsIgnoreCase("animation")){

                    Animation selectedAnimation = DataHolder.getSelectedAnimation(sender.getName());
                    if(selectedAnimation != null){
                        DataHolder.selectAnimation(sender.getName(), null);
                        DataHolder.selectFrame(sender.getName(), null);
                        DataHolder.selectAction(sender.getName(), null);

                        DataHolder.removeAnimation(selectedAnimation.getName());

                        for(AnimationContainer ac : DataHolder.getContainers()){
                            if(ac.getAnimation().equals(selectedAnimation)){
                                DataHolder.getContainers().remove(ac);
                            }
                        }

                        sender.sendMessage("§aDie Animation §e" + selectedAnimation.getName() + " §aund alle zugehörigen Container wurde gelöscht!");
                    }else{
                        sender.sendMessage("§cDu musst zuerst eine Animation auswählen, die du löschen willst!");
                    }

                }else if(args[1].equalsIgnoreCase("frame")){

                    Animation selectedAnimation = DataHolder.getSelectedAnimation(sender.getName());

                    Frame selectedFrame = DataHolder.getSelectedFrame(sender.getName());
                    if(selectedFrame != null){

                        if(DataHolder.getReferenceLocation(sender.getName()) != null){
                            for(ChangedBlock b : DataHolder.getSelectedFrame(sender.getName()).getChangedBlocks()) {
                                Bukkit.getPlayer(sender.getName()).sendBlockChange(DataHolder.getReferenceLocation(sender.getName()).clone().subtract(b.getRelX(), b.getRelY(), b.getRelZ()), b.getBlockType(), (byte) b.getData());
                            }
                            DataHolder.setReferenceLocation(sender.getName(), null);
                        }

                        DataHolder.selectFrame(sender.getName(), null);
                        DataHolder.selectAction(sender.getName(), null);

                        selectedAnimation.getFrames().remove(selectedFrame);

                        sender.sendMessage("§aDer Frame wurde gelöscht!");

                    }else{
                        sender.sendMessage("§cDu musst zuerst eine Animation auswählen, die du löschen willst!");
                    }

                }else if(args[1].equalsIgnoreCase("action")){

                    Frame selectedFrame = DataHolder.getSelectedFrame(sender.getName());

                    Action selectedAction = DataHolder.getSelectedAction(sender.getName());
                    if(selectedAction != null){

                        DataHolder.selectAction(sender.getName(), null);

                        selectedFrame.getActions().remove(selectedAction);

                        sender.sendMessage("§aDie Aktion wurde gelöscht!");

                    }else{
                        sender.sendMessage("§cDu musst zuerst eine Aktion auswählen, die du löschen willst!");
                    }

                }else if(args[1].equalsIgnoreCase("container")){

                    AnimationContainer selectedContainer = DataHolder.getSelectedContainer(sender.getName());
                    if(selectedContainer != null){

                        DataHolder.selectContainer(sender.getName(), null);
                        DataHolder.removeContainer(selectedContainer.getName());

                        sender.sendMessage("§aDer Container §e" + selectedContainer.getName() + " §awurde gelöscht!");

                    }else{
                        sender.sendMessage("§cDu musst zuerst einen Container auswählen, den du löschen willst!");
                    }

                }else{
                    sender.sendMessage("§c/animatedblocks delete <animation/frame/action/container>");
                }

            }else{
                sender.sendMessage("§c/animatedblocks delete <animation/frame/action/container>");
            }

        }else if(args[0].equalsIgnoreCase("play")){

            AnimationContainer container = null;

            if(args.length == 2){
                if(DataHolder.containerExists(args[1])){
                    container = DataHolder.getContainer(args[1]);
                }else{
                    sender.sendMessage("§cEs wurde kein Container mit diesem Namen gefunden!");
                }
            }else{
                container = DataHolder.getSelectedContainer(sender.getName());
            }

            if(container != null){


                if(checkSettings(container)){
                    container.ready();
                    container.resume();
                    sender.sendMessage("§aDer Container §e" + container.getName() + "§a spielt jetzt ab!");
                }else{
                    sender.sendMessage("§cWoops, da ist was mit den Einstellungen des Containers falsch gelaufen. Für Details, siehe Konsole!");
                }
            }else{
                sender.sendMessage("§cDu musst zuerst einen Container auswählen!");
            }

        }else if(args[0].equalsIgnoreCase("pause")){

            AnimationContainer container = null;

            if(args.length == 2){
                if(DataHolder.containerExists(args[1])){
                    container = DataHolder.getContainer(args[1]);
                }else{
                    sender.sendMessage("§cEs wurde kein Container mit diesem Namen gefunden!");
                }
            }else{
                container = DataHolder.getSelectedContainer(sender.getName());
            }

            if(container != null){
                container.pause();
                sender.sendMessage("§aDer Container §e" + container.getName() + "§a wurde pausiert, falls er lief!");
            }

        }else if(args[0].equalsIgnoreCase("resume")) {

            AnimationContainer container = null;

            if (args.length == 2) {
                if (DataHolder.containerExists(args[1])) {
                    container = DataHolder.getContainer(args[1]);
                } else {
                    sender.sendMessage("§cEs wurde kein Container mit diesem Namen gefunden!");
                }
            } else {
                container = DataHolder.getSelectedContainer(sender.getName());
            }

            if (container != null) {

                if (checkSettings(container)) {
                    container.resume();
                    sender.sendMessage("§aDer Container §e" + container.getName() + "§a spielt jetzt ab!");
                } else {
                    sender.sendMessage("§cWoops, da ist was mit den Einstellungen des Containers falsch gelaufen. Für Details, siehe Konsole!");
                }
            }
        }else if(args[0].equalsIgnoreCase("stopedit")){

            if(DataHolder.getReferenceLocation(sender.getName()) != null){
                for(ChangedBlock b : DataHolder.getSelectedFrame(sender.getName()).getChangedBlocks()) {
                    Bukkit.getPlayer(sender.getName()).sendBlockChange(DataHolder.getReferenceLocation(sender.getName()).clone().subtract(b.getRelX(), b.getRelY(), b.getRelZ()), b.getBlockType(), (byte) b.getData());
                }
                DataHolder.setReferenceLocation(sender.getName(), null);
                sender.sendMessage("§aEditmodus geschlossen!");
            }else{
                sender.sendMessage("§cDu musst im EditModus sein, um ihn schließen zu können!");
            }

        }else{
            sender.sendMessage("§c/animatedblocks <create/list/edit/delete/select/play/pause/resume>");
        }


        return true;
    }

    private boolean checkSettings(AnimationContainer container){
        boolean invalidSettings = false;

        if(container.getStartFrame() != -1 || container.getStopFrame() != -1){

            if(container.getStartFrame() >= container.getAnimation().getFrames().size()
                    || container.getStopFrame() >= container.getAnimation().getFrames().size()){
                System.err.println("FEHLER: Einer der beiden Bound-Frame-Indexen ist größer als die Anzahl an Frames! :: " + container.getName());

                invalidSettings = true;
            }

            if(!(container.getStartFrame() < container.getStopFrame())){
                System.err.print("FEHLER: start- und stopBoundFrameIndexe sind nicht in aufsteigender Reihenfolge! ::" + container.getName());

                invalidSettings = true;
            }
        }

        if(container.getLoopMode().equals(LoopMode.WAIT_RANDOM) && (container.getRandomWaitMin() == -1 || container.getRandomWaitMax() == -1)){
            System.err.println("FEHLER: LoopMode ist WAIT_RANDOM, randomWaitMin und randomWaitMax sind aber undefiniert!");

            invalidSettings = true;
        }else if(container.getLoopMode().equals(LoopMode.WAIT_FOR_OFFSET) && (container.getOffset() < 1 || container.getOffset() > 1200)){
            System.err.println("FEHLER: LoopMode ist WAIT_FOR_OFFSET, offset ist aber falsch oder nicht definiert!");

            invalidSettings = true;
        }

        return !invalidSettings;
    }
}
