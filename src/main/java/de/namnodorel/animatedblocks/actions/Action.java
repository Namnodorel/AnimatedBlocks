package de.namnodorel.animatedblocks.actions;

import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

public interface Action extends ConfigurationSerializable {
    void run(Location absolutePosition);
}
