package de.namnodorel.animatedblocks.actions;


import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class SoundAction implements Action{

    private Double selX;
    private Double selY;
    private Double selZ;
    private Integer radius;
    private Sound sound;
    private Double x;
    private Double y;
    private Double z;
    private Float pitch;
    private Float volume;

    public SoundAction(Double selX, Double selY, Double selZ, Integer radius, Sound sound, Double x, Double y, Double z, Float pitch, Float volume) {
        this.selX = selX;
        this.selY = selY;
        this.selZ = selZ;
        this.radius = radius;
        this.sound = sound;
        this.x = x;
        this.y = y;
        this.z = z;
        this.pitch = pitch;
        this.volume = volume;
    }

    @Override
    public void run(Location absolutePosition) {

        for(Player p : Bukkit.getOnlinePlayers()){
            if(p.getLocation().distance(absolutePosition.clone().add(selX, selY, selZ)) <= radius){
                p.playSound(absolutePosition.clone().add(x, y, z), sound, SoundCategory.MASTER, volume, pitch);
            }
        }


    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public Sound getSound() {
        return sound;
    }

    public void setSound(Sound sound) {
        this.sound = sound;
    }

    public Float getPitch() {
        return pitch;
    }

    public void setPitch(Float pitch) {
        this.pitch = pitch;
    }

    public Float getVolume() {
        return volume;
    }

    public void setVolume(Float volume) {
        this.volume = volume;
    }

    public Double getSelectorX() {
        return selX;
    }

    public Double getSelectorY() {
        return selY;
    }

    public Double getSelectorZ() {
        return selZ;
    }

    public Double getX() {
        return x;
    }

    public Double getY() {
        return y;
    }

    public Double getZ() {
        return z;
    }

    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> serialized = new HashMap<>();
        serialized.put("selX", selX);
        serialized.put("selY", selY);
        serialized.put("selZ", selZ);
        serialized.put("radius", radius);
        serialized.put("x", x);
        serialized.put("y", y);
        serialized.put("z", z);
        serialized.put("sound", sound.name());
        serialized.put("volume", volume);
        serialized.put("pitch", pitch);
        return serialized;
    }

    public static SoundAction deserialize(Map<String, Object> serialized){
        return new SoundAction(
                ((Number)serialized.get("selX")).doubleValue(),
                ((Number)serialized.get("selY")).doubleValue(),
                ((Number)serialized.get("selZ")).doubleValue(),
                ((Number)serialized.get("radius")).intValue(),
                Sound.valueOf((String)serialized.get("sound")),
                ((Number)serialized.get("x")).doubleValue(),
                ((Number)serialized.get("y")).doubleValue(),
                ((Number)serialized.get("z")).doubleValue(),
                ((Number)serialized.get("pitch")).floatValue(),
                ((Number)serialized.get("volume")).floatValue()
        );
    }
}
