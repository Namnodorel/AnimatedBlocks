package de.namnodorel.animatedblocks;

import de.namnodorel.animatedblocks.actions.Action;
import de.namnodorel.animatedblocks.actions.SoundAction;
import de.namnodorel.animatedblocks.model.Animation;
import de.namnodorel.animatedblocks.model.ChangedBlock;
import de.namnodorel.animatedblocks.model.Frame;
import de.namnodorel.animatedblocks.player.AnimationContainer;
import de.namnodorel.animatedblocks.setup.AnimatedBlocksCommand;
import de.namnodorel.animatedblocks.player.MainScheduler;
import de.namnodorel.animatedblocks.setup.EditingListener;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.java.JavaPlugin;

public final class AnimatedBlocks extends JavaPlugin {

    private static AnimatedBlocks instance;

    @Override
    public void onEnable() {
        instance = this;

        ConfigurationSerialization.registerClass(Action.class);
        ConfigurationSerialization.registerClass(SoundAction.class);
        ConfigurationSerialization.registerClass(Animation.class);
        ConfigurationSerialization.registerClass(ChangedBlock.class);
        ConfigurationSerialization.registerClass(Frame.class);
        ConfigurationSerialization.registerClass(AnimationContainer.class);

        DataHolder.reload();

        this.getCommand("animatedblocks").setExecutor(new AnimatedBlocksCommand());
        this.getServer().getPluginManager().registerEvents(new EditingListener(), this);

        MainScheduler.start();
    }

    @Override
    public void onDisable() {
        DataHolder.save();
    }

    public static AnimatedBlocks getInstance() {
        return instance;
    }
}
