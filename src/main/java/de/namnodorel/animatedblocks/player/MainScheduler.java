package de.namnodorel.animatedblocks.player;

import de.namnodorel.animatedblocks.AnimatedBlocks;
import de.namnodorel.animatedblocks.DataHolder;
import de.namnodorel.animatedblocks.model.ChangedBlock;
import de.namnodorel.animatedblocks.model.Frame;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class MainScheduler {

    private static Integer count;
    private static Integer taskId;


    public static void start(){

        if(taskId != null){
            return;
        }

        count = 0;

        for(AnimationContainer ac : DataHolder.getContainers()){
            if(ac.getAutoplay()){
                ac.resume();
            }
        }

        taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(AnimatedBlocks.getInstance(), () -> {
            count++;

            //Reset every minute
            if(count > 1200){
                count = 0;
            }

            for(AnimationContainer container : DataHolder.getContainers()){
                container.tick();
            }

            //Every half a second, blink on the selection
            if(count%10 == 0){
                for(String player : DataHolder.referenceLocations.keySet()){
                    Frame selectedFrame = DataHolder.getSelectedFrame(player);
                    boolean marked = count%20 == 0;

                    for(ChangedBlock b : selectedFrame.getChangedBlocks()){
                        if(marked){
                            Bukkit.getPlayer(player).sendBlockChange(DataHolder.getReferenceLocation(player).clone().subtract(b.getRelX(), b.getRelY(), b.getRelZ()), Material.STAINED_GLASS, (byte)4);
                        }else{
                            DataHolder.getReferenceLocation(player).clone().subtract(b.getRelX(), b.getRelY(), b.getRelZ()).getBlock().setType(b.getBlockType());
                            DataHolder.getReferenceLocation(player).clone().subtract(b.getRelX(), b.getRelY(), b.getRelZ()).getBlock().setData(b.getData());
                            Bukkit.getPlayer(player).sendBlockChange(DataHolder.getReferenceLocation(player).clone().subtract(b.getRelX(), b.getRelY(), b.getRelZ()), b.getBlockType(), b.getData());

                        }
                    }
                }
            }

        }, 0L, 1L);
    }

    public static Integer getCount() {
        return count;
    }
}
