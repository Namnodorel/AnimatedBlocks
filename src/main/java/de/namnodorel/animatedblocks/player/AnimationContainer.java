package de.namnodorel.animatedblocks.player;

import de.namnodorel.animatedblocks.DataHolder;
import de.namnodorel.animatedblocks.model.Animation;
import de.namnodorel.animatedblocks.model.ChangedBlock;
import de.namnodorel.animatedblocks.model.Frame;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class AnimationContainer implements ConfigurationSerializable{

    private String name;
    private Animation animation;
    private Location absolutePosition;
    private Boolean autoplay;

    private LoopMode loopMode;
    private Integer offset; //1-1200 or -1

    private Integer startFrame;//Inclusive
    private Integer stopFrame;//Inclusive
    private long randomWaitMin;
    private long randomWaitMax;

    private boolean paused;
    private long ticksPlayed;
    private boolean waitingForOffset;
    private long waitingForTicks;

    public AnimationContainer(String name, Animation animation, Location absolutePosition) {
        this.name = name;
        this.animation = animation;
        this.absolutePosition = absolutePosition;
        autoplay = false;

        //default values
        loopMode = LoopMode.CONTINUE_INSTANTLY;
        offset = -1;
        startFrame = -1;
        stopFrame = -1;
        randomWaitMin = -1;
        randomWaitMax = -1;

        paused = true;
        ticksPlayed = 0;
        waitingForOffset = true;
        waitingForTicks = 0;
    }

    public void ready(){
        waitingForOffset = true;
        ticksPlayed = 0;
        waitingForTicks = 0;
    }

    public void tick(){

        if(!isPaused()){
            if(waitingForOffset && offset > 0 && !MainScheduler.getCount().equals(offset)){
                return;
            }else{
                waitingForOffset = false;
            }

            if(waitingForTicks == 0){
                Frame newFrame = null;

                long ticks = 0;
                for(Frame frame : animation.getFrames()){
                    if((startFrame == -1 || animation.getFrames().indexOf(frame) >= startFrame)
                            && (stopFrame == -1 || animation.getFrames().indexOf(frame) <= stopFrame)){
                        if(ticksPlayed == ticks){
                            newFrame = frame;
                            break;
                        }else{
                            ticks += frame.getTimeUntilNext()*20;
                        }
                    }
                }

                if(newFrame != null){

                    for(ChangedBlock block : newFrame.getChangedBlocks()){
                        absolutePosition.clone().subtract(block.getRelX(), block.getRelY(), block.getRelZ()).getBlock().setType(block.getBlockType());
                        absolutePosition.clone().subtract(block.getRelX(), block.getRelY(), block.getRelZ()).getBlock().setData(block.getData());
                    }

                    newFrame.runActions(absolutePosition);

                }else if(ticksPlayed > ticks){
                    finish();
                }

                ticksPlayed++;
            }else{
                waitingForTicks--;
            }
        }
    }

    private void finish(){

        //If we'd set it to 0, the first frame wouldn't be shown on repitions

        if(loopMode == LoopMode.CONTINUE_INSTANTLY){
            ticksPlayed = -1;

        }else if(loopMode == LoopMode.WAIT_FOR_OFFSET){
            ticksPlayed = -1;
            waitingForOffset = true;

        }else if(loopMode == LoopMode.WAIT_RANDOM){
            ticksPlayed = -1;
            waitingForTicks = ThreadLocalRandom.current().nextLong(randomWaitMin, randomWaitMax + 1);
        }else if(loopMode == LoopMode.NONE){
            ticksPlayed = -1;
            waitingForOffset = true;
            pause();
        }

    }

    public void setLoopMode(LoopMode loopMode) {
        this.loopMode = loopMode;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public void setStartFrame(Integer startFrame) {
        this.startFrame = startFrame;
    }

    public void setStopFrame(Integer stopFrame) {
        this.stopFrame = stopFrame;
    }

    public void pause() {
        this.paused = true;
    }

    public void resume(){
        this.paused = false;
    }

    public Animation getAnimation() {
        return animation;
    }

    public Location getAbsolutePosition() {
        return absolutePosition;
    }

    public LoopMode getLoopMode() {
        return loopMode;
    }

    public Integer getOffset() {
        return offset;
    }

    public Integer getStartFrame() {
        return startFrame;
    }

    public Integer getStopFrame() {
        return stopFrame;
    }

    public Long getRandomWaitMin() {
        return randomWaitMin;
    }

    public Long getRandomWaitMax() {
        return randomWaitMax;
    }

    public boolean isPaused() {
        return paused;
    }

    public long getTicksPlayed() {
        return ticksPlayed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAnimation(Animation animation) {
        this.animation = animation;
    }

    public void setAbsolutePosition(Location absolutePosition) {
        this.absolutePosition = absolutePosition;
    }

    public void setRandomWaitMin(long randomWaitMin) {
        this.randomWaitMin = randomWaitMin;
    }

    public void setRandomWaitMax(long randomWaitMax) {
        this.randomWaitMax = randomWaitMax;
    }

    public Boolean getAutoplay() {
        return autoplay;
    }

    public void setAutoplay(Boolean autoplay) {
        this.autoplay = autoplay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AnimationContainer)) return false;

        AnimationContainer that = (AnimationContainer) o;

        if (getRandomWaitMin() != that.getRandomWaitMin()) return false;
        if (getRandomWaitMax() != that.getRandomWaitMax()) return false;
        if (!getName().equals(that.getName())) return false;
        if (!getAnimation().equals(that.getAnimation())) return false;
        if (!getAbsolutePosition().equals(that.getAbsolutePosition())) return false;
        if (getLoopMode() != that.getLoopMode()) return false;
        if (!getOffset().equals(that.getOffset())) return false;
        if (!getStartFrame().equals(that.getStartFrame())) return false;
        return getStopFrame().equals(that.getStopFrame());
    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + getAnimation().hashCode();
        result = 31 * result + getAbsolutePosition().hashCode();
        result = 31 * result + getLoopMode().hashCode();
        result = 31 * result + getOffset().hashCode();
        result = 31 * result + getStartFrame().hashCode();
        result = 31 * result + getStopFrame().hashCode();
        result = 31 * result + (int) (getRandomWaitMin() ^ (getRandomWaitMin() >>> 32));
        result = 31 * result + (int) (getRandomWaitMax() ^ (getRandomWaitMax() >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "AnimationContainer{" +
                "name='" + name + '\'' +
                ", animation=" + animation +
                ", absolutePosition=" + absolutePosition +
                ", loopMode=" + loopMode +
                ", offset=" + offset +
                ", startFrame=" + startFrame +
                ", stopFrame=" + stopFrame +
                ", randomWaitMin=" + randomWaitMin +
                ", randomWaitMax=" + randomWaitMax +
                ", paused=" + paused +
                ", ticksPlayed=" + ticksPlayed +
                ", waitingForOffset=" + waitingForOffset +
                ", waitingForTicks=" + waitingForTicks +
                '}';
    }

    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> serialized = new HashMap<>();
        serialized.put("name", name);
        serialized.put("animation", animation.getName());
        serialized.put("position", absolutePosition);
        serialized.put("autoplay", autoplay);
        serialized.put("loopMode", loopMode.name());
        serialized.put("offset", offset);
        serialized.put("startFrame", startFrame);
        serialized.put("stopFrame", stopFrame);
        serialized.put("randomWaitMin", randomWaitMax);
        serialized.put("randomWaitMax", randomWaitMax);
        return serialized;
    }

    public static AnimationContainer deserialize(Map<String, Object> serialized){
        AnimationContainer deserialized = new AnimationContainer(
                (String)serialized.get("name"),
                DataHolder.getAnimation((String)serialized.get("animation")),
                (Location)serialized.get("position"));
        deserialized.autoplay = (Boolean)serialized.get("autoplay");
        deserialized.loopMode = LoopMode.valueOf((String)serialized.get("loopMode"));
        deserialized.offset = (Integer)serialized.get("offset");
        deserialized.startFrame = (Integer)serialized.get("startFrame");
        deserialized.startFrame = (Integer)serialized.get("stopFrame");
        deserialized.randomWaitMin = ((Number)serialized.get("randomWaitMin")).longValue();
        deserialized.randomWaitMax = ((Number)serialized.get("randomWaitMax")).longValue();

        return deserialized;
    }
}
