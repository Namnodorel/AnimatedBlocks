package de.namnodorel.animatedblocks.player;

public enum LoopMode {
    NONE,
    CONTINUE_INSTANTLY,
    WAIT_FOR_OFFSET,
    WAIT_RANDOM
}
